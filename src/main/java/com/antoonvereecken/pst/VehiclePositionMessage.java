package com.antoonvereecken.pst;

import java.util.Date;

public class VehiclePositionMessage {

    private final int vehicleId;
    private final Date currentDateTime;
    private final int longPosition;
    private final int latPosition;

    public VehiclePositionMessage(int vehicleId, Date currentDateTime, int longPosition, int latPosition) {
        this.vehicleId = vehicleId;
        this.currentDateTime = currentDateTime;
        this.longPosition = longPosition;
        this.latPosition = latPosition;
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public Date getCurrentDateTime() {
        return currentDateTime;
    }

    public int getLongPosition() {
        return longPosition;
    }

    public int getLatPosition() {
        return latPosition;
    }

    @Override
    public String toString() {
        return "VehiclePositionMessage{" +
                "vehicleId=" + vehicleId +
                ", currentDateTime=" + currentDateTime +
                ", longPosition=" + longPosition +
                ", latPosition=" + latPosition +
                '}';
    }
}
