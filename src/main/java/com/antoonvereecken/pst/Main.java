package com.antoonvereecken.pst;

import akka.NotUsed;
import akka.actor.typed.ActorSystem;
import akka.actor.typed.javadsl.Behaviors;
import akka.stream.*;
import akka.stream.javadsl.*;
import scala.Int;

import java.time.Duration;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/** "//" = instructions, not descriptions */
public class Main {
    public static void main(String[] args) {
        ActorSystem<?> actorSystem = ActorSystem.create(Behaviors.empty(), "actorSystem");
        Map<Integer, VehiclePositionMessage> vehicleTrackingMap = new HashMap<>();
        for (int i = 1; i <=8; i++) {
            vehicleTrackingMap.put(i, new VehiclePositionMessage(1, new Date(), 0,0));
        }

        //source - repeat some value every 10 seconds.
        Source<Integer, NotUsed> source = Source.repeat(1).throttle(1, Duration.ofSeconds(10));

        //flow 1 - transform into the ids of each van (ie 1..8) with mapConcat
        Flow<Integer, Integer, NotUsed> signalToVanIdsFlow = Flow.of(Integer.class)
                .mapConcat(val -> List.of(1, 2, 3, 4, 5, 6, 7, 8));

        //flow 2 - get position for each van as a VPMs with a call to the lookup method (create a new instance of
        //utility functions each time). Note that this process isn't instant so should be run in parallel.
        Flow<Integer, VehiclePositionMessage, NotUsed> vanIdToVPMsgAsyncFlow = Flow.of(Integer.class)
                .mapAsyncUnordered(8, vanId -> {
                    actorSystem.log().info(String.format("Requesting position for vanId: %d", vanId));
                    CompletableFuture<VehiclePositionMessage> futureVPMsg = new CompletableFuture<>();
                    UtilityFunctions utilityFunctions = new UtilityFunctions();
                    futureVPMsg.completeAsync( () -> utilityFunctions.getVehiclePosition(vanId));
                    return futureVPMsg;
                });

        //flow 3 - use previous position from the map to calculate the current speed of each vehicle. Replace the
        // position in the map with the newest position and pass the current speed downstream
        Flow<VehiclePositionMessage, VehicleSpeed, NotUsed> vanPositionToSpeedFlow = Flow.of(VehiclePositionMessage.class)
                .map(vPM -> {
                    UtilityFunctions utilFun = new UtilityFunctions();
                    VehiclePositionMessage prevVPMsg = vehicleTrackingMap.get(vPM.getVehicleId());
                    VehicleSpeed currentSpeed = utilFun.calculateSpeed(vPM, prevVPMsg);
                    actorSystem.log().info(String.format(
                            "Vehicle %d is travelling at %f", vPM.getVehicleId(), currentSpeed.getSpeed()));
                    vehicleTrackingMap.put(vPM.getVehicleId(), vPM);
                    return currentSpeed;
                });

        //flow 4 - filter to only keep those values with a speed > 95
        Flow<VehicleSpeed, VehicleSpeed, NotUsed> speedFilterFlow = Flow.of(VehicleSpeed.class)
                .filter(e -> (e.getSpeed() > 95));
                /* .take(1); like Sink.head(), takes first element & closes stream */

        //sink - as soon as 1 value is received return it as a materialized value, and terminate the stream
        /* take 1 (first) & complete graph, return as a materialized value */
        Sink<VehicleSpeed, CompletionStage<VehicleSpeed>> sink = Sink.head();

//        CompletionStage<VehicleSpeed> result = source.via(signalToVanIdsFlow)
//                .async()
//                .via(vanIdToVPMsgAsyncFlow)
//                .async()
//                .via(vanPositionToSpeedFlow)
//                .via(speedFilterFlow)
//                .toMat(sink, Keep.right())
//                .run(actorSystem);

        RunnableGraph<CompletionStage<VehicleSpeed>> graph = RunnableGraph.fromGraph(
            GraphDSL.create(sink, (builder, out) -> {
//                    SourceShape<Integer> constructedSourceShape = builder.add(
//                            Source.repeat(1).throttle(1, Duration.ofSeconds(10)));
                SourceShape<Integer> sourceShape = builder.add(source);
                FlowShape<Integer, Integer> vIdFlowS = builder.add(
                        signalToVanIdsFlow.async()
                // !=`.via(flow).async()` -> does not create boundaries in the stream, but dedicates own threads
                );
                /* using fan instead of next line*/
//                FlowShape<Integer, VehiclePositionMessage> vPMsgFlowS = builder.add(vanIdToVPMsgAsyncFlow);
                FlowShape<VehiclePositionMessage, VehicleSpeed> vSpeedFlowS = builder.add(vanPositionToSpeedFlow);
                FlowShape<VehicleSpeed, VehicleSpeed> speedFilterFlowS = builder.add(speedFilterFlow);
                // we don't need to add a SinkShape -> this is taken care of by GraphDSL.create(sink, (builder, out)..
//                SinkShape<VehicleSpeed> sinkShape = builder.add(sink);

                UniformFanOutShape<Integer, Integer> balanceUniFanOut = builder.add(
                        Balance.create(8, true));
                UniformFanInShape<VehiclePositionMessage, VehiclePositionMessage> mergeUniFanIn = builder.add(
                        Merge.create(8));

//                builder.from(sourceShape)                     // S > F_1 > F_2 > F_3 > F_4 > S
//                          .via(vIdFlowS)
//                          .via(vPMsgFlowS)
//                          .via(vSpeedFlowS)
//                          .via(speedFilterFlowS)
//                        .to(out);

                builder.from(sourceShape)                   // S > Flow_1 > FanOut(8)
                            .via(vIdFlowS)
                            .viaFanOut(balanceUniFanOut);

                for (int i = 0; i < 8 ;i++){                // FanOut > 8x FanIn >
                    builder.from(balanceUniFanOut)
                            .via(builder.add(vanIdToVPMsgAsyncFlow.async()))
                        .toFanIn(mergeUniFanIn);
                }
//                builder.from(vSpeedFlowS)                   // F_3 > F_4 > S
//                            .via(speedFilterFlowS)
//                        .to(out);
                builder.from(mergeUniFanIn)                    // F_2 > F_3
                            .via(vSpeedFlowS)
                            .via(speedFilterFlowS)
                        .to(out);
                return ClosedShape.getInstance();
            })
        );
        CompletionStage<VehicleSpeed> result = graph.run(actorSystem);

        result.whenComplete((vehicleSpeed, throwable) -> {
           if (throwable == null) {
               actorSystem.log().info(String.format("Vehicle %d was going %f !!! Closing app...", vehicleSpeed.getVehicleId(), vehicleSpeed.getSpeed()));
               vehicleTrackingMap.forEach((vanId, vehiclePositionMessage) ->
                   actorSystem.log().info(String.format("%d : %s", vanId, vehiclePositionMessage.toString()))
               );
               actorSystem.terminate();
           } else actorSystem.log().error("Oops... Something went wrong.... \n", throwable);
        });

    }

}
