package com.antoonvereecken.pst;

import akka.Done;
import akka.NotUsed;
import akka.actor.typed.ActorSystem;
import akka.actor.typed.javadsl.Behaviors;
import akka.stream.*;
import akka.stream.javadsl.*;

import java.util.concurrent.CompletionStage;

public class ComplexFlowTypes {
    public static void main(String[] args) {
        ActorSystem<?> actorSys = ActorSystem.create(Behaviors.empty(), "actorSystem");

        Source<Integer, NotUsed> source = Source.range(1, 10);
        Flow<Integer, Integer, NotUsed> flow1 = Flow.of(Integer.class)
                .map(x -> {
                    System.out.println("Flow1 is processing " + x);
                    return x * 10;
                });
        Flow<Integer, Integer, NotUsed> flow2 = Flow.of(Integer.class)
                .map(x -> {
                    System.out.println("Flow2 is processing " + x);
                    return x * 100;
                });
        Sink<Integer, CompletionStage<Done>> sink = Sink.foreach(System.out::println);

        RunnableGraph<CompletionStage<Done>> graph = RunnableGraph.fromGraph(
                GraphDSL.create(sink, (builder, out) -> {
                    SourceShape<Integer> sourceShape = builder.add(source);
                    FlowShape<Integer, Integer> flow1FShape = builder.add(flow1);
                    FlowShape<Integer, Integer> flow2FShape = builder.add(flow2);
                    UniformFanOutShape<Integer, Integer> broadCastUniFanOut = builder.add(
                            Broadcast.create(2));
                    UniformFanInShape<Integer, Integer> mergeUniFanIn = builder.add(
                            Merge.create(2));

//                    builder.from(sourceShape)                           // Source > FanOut(2)
//                            .toFanOut(broadCastUniFanOut);
//                    builder.from(broadCastUniFanOut.out(0))             // FanOut[0] > Flow_1
//                                .via(flow1FShape);
//                    builder.from(broadCastUniFanOut.out(1))             // FanOut[1] > Flow_2
//                                .via(flow2FShape);
//                    builder.from(flow1FShape)                           // Flow_1 > FanIn[0]
//                            .toInlet(mergeUniFanIn.in(0));
//                    builder.from(flow2FShape)                           // Flow_2 > FanIn[1]
//                            .toInlet(mergeUniFanIn.in(1));
//                    builder.from(mergeUniFanIn)                         // Fan_In(2) > Sink
//                            .to(out);
                    builder.from(sourceShape)
                                .viaFanOut(broadCastUniFanOut)
                                        .via(flow1FShape);
                    builder.from(broadCastUniFanOut)
                                        .via(flow2FShape);
                    builder.from(flow1FShape)
                                .viaFanIn(mergeUniFanIn);
                    builder.from(flow2FShape)
                                .viaFanIn(mergeUniFanIn)
                            .to(out);


                    return ClosedShape.getInstance();
                }));
        graph.run(actorSys);
    }
}
