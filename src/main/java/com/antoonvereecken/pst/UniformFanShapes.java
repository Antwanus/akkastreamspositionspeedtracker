package com.antoonvereecken.pst;

import akka.Done;
import akka.actor.typed.ActorSystem;
import akka.actor.typed.javadsl.Behaviors;
import akka.stream.*;
import akka.stream.javadsl.*;

import java.time.Duration;
import java.util.Random;
import java.util.concurrent.CompletionStage;

public class UniformFanShapes {
    public static void main(String[] args) {
        ActorSystem<?> actorSys = ActorSystem.create(Behaviors.empty(), "actorSystem");

        RunnableGraph<CompletionStage<Done>> graph = RunnableGraph.fromGraph(
            GraphDSL.create(Sink.foreach(System.out::println),
                (builder, out) -> {
                    SourceShape<Integer> intSourceShape = builder.add(
                            Source.repeat(1)
                                    .throttle(1, Duration.ofSeconds(1))
                                    .map(x -> {
                                        Random r = new Random();
                                        return 1 + r.nextInt(10);
                                    })
                    );
                    FlowShape<Integer, Integer> intIntFlowShape = builder.add(
                            Flow.of(Integer.class)
                                    .map(x -> {
                                        System.out.println("x (" + x + ") is flowing");
                                        return x;
                                    })
                    );
                    UniformFanOutShape<Integer, Integer> partitionFanOut = builder.add(
                            Partition.create(2,
                                    x -> (x == 1 || x == 10) ? 0 : 1)
                    );
                    UniformFanInShape<Integer, Integer> mergeFanIn = builder.add(
                            Merge.create(2)
                    );

                    builder.from(intSourceShape)
                                .viaFanOut(partitionFanOut);
                    builder.from(partitionFanOut.out(0))
                                .via(intIntFlowShape)
                                .viaFanIn(mergeFanIn)
                                .to(out);
                    builder.from(partitionFanOut.out(1))
                                .viaFanIn(mergeFanIn);

                    return ClosedShape.getInstance();
                })
        );
        graph.run(actorSys);

    }
}
