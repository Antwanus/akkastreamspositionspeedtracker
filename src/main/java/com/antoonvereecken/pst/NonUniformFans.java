package com.antoonvereecken.pst;

import akka.Done;
import akka.actor.typed.ActorSystem;
import akka.actor.typed.javadsl.Behaviors;
import akka.japi.tuple.Tuple3;
import akka.stream.*;
import akka.stream.javadsl.*;

import java.util.concurrent.CompletionStage;

public class NonUniformFans {
    public static void main(String[] args) {
        ActorSystem<?> actorSys = ActorSystem.create(Behaviors.empty(), "actorSystem");

        RunnableGraph<CompletionStage<Done>> graph = RunnableGraph.fromGraph(
            GraphDSL.create(Sink.foreach(System.out::println),
                (builder, out) -> {
                    SourceShape<Integer> sourceShape = builder.add(Source.range(1, 10));

                    FlowShape<Integer,Integer> intFlowShape = builder.add(Flow.of(Integer.class)
                            .map(x -> {
                                System.out.println("IntegerFlow: " + x);
                                return x;
                    }));
                    FlowShape<Boolean,Boolean> boolFlowShape = builder.add(Flow.of(Boolean.class)
                            .map(b -> {
                                System.out.println("BooleanFlow: " + b);
                                return b;
                    }));
                    FlowShape<String,String> stringFlowShape = builder.add(Flow.of(String.class)
                            .map(s -> {
                                System.out.println("StringFlow: " + s);
                                return s;
                    }));

                    FanOutShape3<Integer, Integer, Boolean, String> fanOutShape3 = builder.add(
                            UnzipWith.create3(input -> new Tuple3<>(
                                    input,                  // Integer
                                    (input % 2 == 0),       // Boolean
                                    "it's " + input)        // String
                    ));
                    FanInShape3<Integer, Boolean, String, String> fanInShape3 = builder.add(
                            ZipWith.create3((x, b, s) -> {
                                StringBuilder sb = new StringBuilder();
                                sb.append("Number: " + x);
                                sb.append(", dividable by 2 : " + b);
                                sb.append(", toString: " + s);
                                return sb.toString();
                            })
                    );

                    builder.from(sourceShape).toInlet(fanOutShape3.in());
                    builder.from(fanOutShape3.out0()).via(intFlowShape).toInlet(fanInShape3.in0());
                    builder.from(fanOutShape3.out1()).via(boolFlowShape).toInlet(fanInShape3.in1());
                    builder.from(fanOutShape3.out2()).via(stringFlowShape).toInlet(fanInShape3.in2());
                    builder.from(fanInShape3.out()).to(out);


                    return ClosedShape.getInstance();
            })
        );
        graph.run(actorSys);


    }
}
