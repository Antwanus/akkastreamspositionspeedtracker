package com.antoonvereecken.pst;

import akka.Done;
import akka.NotUsed;
import akka.actor.typed.ActorSystem;
import akka.actor.typed.javadsl.Behaviors;
import akka.event.slf4j.Slf4jLogger;
import akka.stream.*;
import akka.stream.javadsl.*;

import java.util.concurrent.CompletionStage;

public class ExploringConcurrency {
    public static void main(String[] args) {
        ActorSystem<?> actorSys = ActorSystem.create(Behaviors.empty(), "actorSystem");

        Source<Integer, NotUsed> source = Source.range(1, 10);

        Flow<Integer, Integer, NotUsed> flow = Flow.of(Integer.class)
                .map( x -> {
                    System.out.println("Starting flow " + x);
                    try {
                        Thread.sleep(3000);
                    } catch (Exception e) { e.printStackTrace(); }
                    System.out.println("Ending flow " + x);
                    return x;
                });

        Sink<Integer, CompletionStage<Done>> sink = Sink.foreach(System.out::println);

        Long start = System.currentTimeMillis();

        final int threadCount = Math.abs(Runtime.getRuntime().availableProcessors() / 2);
        RunnableGraph<CompletionStage<Done>> graph = RunnableGraph.fromGraph(
                GraphDSL.create(sink, (builder, out) -> {
                    SourceShape<Integer> sourceShape = builder.add(source);
//                    FlowShape<Integer, Integer> flowShape = builder.add(flow);
                    UniformFanOutShape<Integer, Integer> balanceUniFanOut = builder.add(
                            Balance.create(threadCount,
                                    true // !!!direct as soon as 1 is rdy, NOT wait for all!!!
                            ));
                    UniformFanInShape<Integer, Integer> mergeUniFanIn = builder.add(
                            Merge.create(threadCount)
                    );

                    builder.from(sourceShape).viaFanOut(balanceUniFanOut);
                    for (int i = 0; i < threadCount; i++) {
                        builder.from(balanceUniFanOut)
                                        .via(builder.add(flow.async()))
                                .toFanIn(mergeUniFanIn);
                    }
                    builder.from(mergeUniFanIn).to(out);

                    return ClosedShape.getInstance();
                })
        );
        CompletionStage<Done> result = graph.run(actorSys);

        result.whenComplete( (done, throwable) -> {
            Long end = System.currentTimeMillis();
            System.out.println("Elapsed time in ms = " + (end-start));
            actorSys.terminate();
        });

    }
}
